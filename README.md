# VolSnapRot

A basic tool made in 2017 that takes a given LVM logical volume and creates snapshots and rotates them automatically whenever a new one is created. Note this tool is largely made redundant by filesystems like ZFS paired with a tool like Sanoid which are much faster and comprehensive. This script is largely kept for posterity and will likely not be maintained.
